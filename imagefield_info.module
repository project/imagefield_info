<?php

/**
 * Implements hook_menu().
 */

function imagefield_info_menu() {
  $items[] = array();

  $items['admin/config/media/image-info'] = array(
    'title' => 'Information info settings',
    'description' => 'Settings for imagefield additional information.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('imagefield_info_settings'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Callback for the admin settings form.
 * @return mixed
 */
function imagefield_info_settings() {
  $form = array();
  $styles = image_styles();
  $list = array();
  foreach ($styles as $key => $style) {
    $list[$key] = $style['label'];
  }

  $form['imagefield_info_styles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed image styles'),
    '#default_value' => variable_get('imagefield_info_styles', array()),
    '#options' => $list,
    '#description' => t("Select the styles you want to display the URL of with the imagefield widget."),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_theme_registry_alter().
 */
function imagefield_info_theme_registry_alter(&$theme_registry) {
  if (!empty($theme_registry['image_widget']['function'])) {
    $theme_registry['image_widget']['function'] = 'imagefield_info_theme_image_widget';
  }
}

/**
 * Image field widget callback.
 */
function imagefield_info_theme_image_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="image-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="image-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data">';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  if ($variables['element']['#file']) {
    $selected_styles = variable_get('imagefield_info_styles', array());
    $all_styles = image_styles();
    $styles = array();
    foreach ($all_styles as $key => $style) {
      if ($selected_styles[$key]) {
        $styles[$key] = array(
          'label' => check_plain($style['label']),
          'url' => image_style_url($key, $variables['element']['#file']->uri),
        );
      }
    }
    $styles['original'] = array(
      'label' => t('Original'),
      'url' => file_create_url($variables['element']['#file']->uri),
    );
    $element['filename']['#markup'] .= theme('imagefield_info', array('styles' => $styles));
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Implements hook_theme().
 */
function imagefield_info_theme($existing, $type, $theme, $path) {
  return array(
    'imagefield_info' => array(
      'template' => 'imagefield-info',
      'path' => drupal_get_path('module', 'imagefield_info') . '/templates',
      'variables' => array(
        'styles' => NULL,
      ),
    ),
  );
}
